package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setup() {
        this.mongoTemplate.dropCollection(User.class);
    }

    @After
    public void tearDown() {
        this.mongoTemplate.dropCollection(User.class);
    }

    @Test
    public void findByMailTest() {
        User user = new User();
        user.setName("John Doe");
        user.setEmail("john.doe@foo.com");
        this.userRepository.save(user);
        User found = this.userRepository.findByEmail(user.getEmail());
        Assertions.assertThat(found).isEqualTo(user);
    }
}
