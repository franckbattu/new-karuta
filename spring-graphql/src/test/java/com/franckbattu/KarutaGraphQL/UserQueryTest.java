package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.UserRepository;
import com.franckbattu.KarutaGraphQL.resolvers.UserQuery;
import com.franckbattu.KarutaGraphQL.security.PasswordDecrypter;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserQueryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setup() {
        this.mongoTemplate.dropCollection(User.class);
    }

    @After
    public void tearDown() {
        this.mongoTemplate.dropCollection(User.class);
    }

    @Test
    public void loginTest() {
        PasswordDecrypter passwordDecrypter = new PasswordDecrypter();
        User user = new User();
        user.setName("John Doe");
        user.setEmail("john.doe@foo.com");
        user.setPassword(passwordDecrypter.crypt("John Doe"));
        user.setRole("Student");

        this.userRepository.save(user);

        UserQuery userQuery = new UserQuery(this.userRepository);

        Assertions.assertThat(userQuery.login("john.doe@foo.com", "John Doe")).isEqualTo(user);
        Assertions.assertThat(userQuery.login("john.doe@foo.com", "Wrong Password")).isEqualTo(null);
        Assertions.assertThat(userQuery.login("Wrong email", "John Doe")).isEqualTo(null);

    }
}
