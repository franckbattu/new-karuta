package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setup() {
        this.mongoTemplate.dropCollection(Student.class);
    }

    @After
    public void tearDown() {
        this.mongoTemplate.dropCollection(Student.class);
    }

    @Test
    public void findByNameTest() {
        Student student = new Student();
        User user = new User();
        user.setName("John Doe");
        student.setData(user);
        this.studentRepository.save(student);
        Student found = this.studentRepository.findStudentByData_Name(student.getData().getName());
        assertThat(found).isEqualTo(student);
    }

    @Test
    public void findByMailTest() {
        Student student = new Student();
        User user = new User();
        user.setEmail("john.doe@foo.com");
        student.setData(user);
        this.studentRepository.save(student);
        Student found = this.studentRepository.findStudentByData_Email(student.getData().getEmail());
        assertThat(found).isEqualTo(student);
    }
}
