package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.security.PasswordDecrypter;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PasswordDecrypterTest {

    private PasswordDecrypter passwordDecrypter;

    @Before
    public void setup() {
        this.passwordDecrypter = new PasswordDecrypter();
    }

    @Test
    public void correctCryptTest() {
        String password = "John Doe";
        String passwordCrypted = this.passwordDecrypter.crypt(password);
        Assertions.assertThat(this.passwordDecrypter.decrypt(password, passwordCrypted)).isEqualTo(true);
        Assertions.assertThat(this.passwordDecrypter.decrypt("Jane Doe", passwordCrypted)).isEqualTo(false);
    }
}
