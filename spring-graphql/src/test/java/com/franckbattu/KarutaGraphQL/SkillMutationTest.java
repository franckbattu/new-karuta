package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import com.franckbattu.KarutaGraphQL.resolvers.SkillMutation;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SkillMutationTest {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setup() {
        this.mongoTemplate.dropCollection(Student.class);
    }

    @After
    public void tearDown() {
        this.mongoTemplate.dropCollection(Student.class);
    }

    @Test
    public void addSkillTest() {
        Student student = new Student();
        User user = new User();
        user.setId("1");
        user.setName("John Doe");
        user.setEmail("john.doe@foo.com");
        student.setData(user);
        student.setSkills(new ArrayList<>());
        this.studentRepository.save(student);

        SkillMutation skillMutation = new SkillMutation(this.studentRepository);
        skillMutation.addSkill(user.getId(), "Skill title", "Skill description", "Skill category", "SkillTechonologies", 4);
        Student found = this.studentRepository.findStudentByData_Id("1");

        Assertions.assertThat(found.getSkills().size()).isEqualTo(student.getSkills().size() + 1);

        Assertions.assertThat(found.getSkills().get(0).getTitle()).isEqualTo("Skill title");
        Assertions.assertThat(found.getSkills().get(0).getDescription()).isEqualTo("Skill description");
        Assertions.assertThat(found.getSkills().get(0).getCategory()).isEqualTo("Skill category");
        Assertions.assertThat(found.getSkills().get(0).getTechnologies()).isEqualTo(List.of("SkillTechonologies"));

    }
}
