package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.models.Teacher;
import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.TeacherRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TeacherRepositoryTest {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setup() {
        this.mongoTemplate.dropCollection(Teacher.class);
    }

    @After
    public void tearDown() {
        this.mongoTemplate.dropCollection(Teacher.class);
    }

    @Test
    public void findByEmailTest() {
        Teacher teacher = new Teacher();
        User user = new User();
        user.setName("Jane Doe");
        user.setEmail("jane.doe@foo.com");
        teacher.setData(user);
        this.teacherRepository.save(teacher);
        Teacher found = this.teacherRepository.findByData_Email(teacher.getData().getEmail());
        assertThat(found).isEqualTo(teacher);
    }
}
