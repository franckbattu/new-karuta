package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import com.franckbattu.KarutaGraphQL.resolvers.StudentQuery;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataMongoTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StudentQueryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setup() {
        this.mongoTemplate.dropCollection(Student.class);
    }

    @After
    public void tearDown() {
        this.mongoTemplate.dropCollection(Student.class);
    }

    @Test
    public void studentByName() {
        Student student = new Student();
        User user = new User();
        user.setName("John Doe");
        user.setEmail("john.doe@foo.com");
        student.setData(user);
        this.studentRepository.save(student);
        StudentQuery studentQuery = new StudentQuery(this.studentRepository);
        Student found = studentQuery.studentByName(user.getName());
        Assertions.assertThat(found).isEqualTo(student);
    }

    @Test
    public void studentByEmail() {
        Student student = new Student();
        User user = new User();
        user.setName("John Doe");
        user.setEmail("john.doe@foo.com");
        student.setData(user);
        this.studentRepository.save(student);
        StudentQuery studentQuery = new StudentQuery(this.studentRepository);
        Student found = studentQuery.studentByEmail(user.getEmail());
        Assertions.assertThat(found).isEqualTo(student);
    }
}
