package com.franckbattu.KarutaGraphQL;

import com.franckbattu.KarutaGraphQL.models.Skill;
import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.models.Teacher;
import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import com.franckbattu.KarutaGraphQL.repositories.TeacherRepository;
import com.franckbattu.KarutaGraphQL.repositories.UserRepository;
import com.franckbattu.KarutaGraphQL.security.PasswordDecrypter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.boot.CommandLineRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CommandLineRunnerApp implements CommandLineRunner {

	private StudentRepository studentRepository;
	private UserRepository userRepository;
	private TeacherRepository teacherRepository;
	private MongoTemplate mongoTemplate;

	public CommandLineRunnerApp(MongoTemplate mongoTemplate, StudentRepository studentRepository, UserRepository userRepository, TeacherRepository teacherRepository) {
        this.mongoTemplate = mongoTemplate;
        this.studentRepository = studentRepository;
        this.userRepository = userRepository;
        this.teacherRepository = teacherRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		this.mongoTemplate.getDb().drop();

		PasswordDecrypter passwordDecrypter = new PasswordDecrypter();

		User user = new User();
		user.setName("John Doe");
		user.setPassword(passwordDecrypter.crypt("JohnDoe"));
		user.setEmail("john.doe@foo.com");
		user.setRole("Student");
		this.userRepository.save(user);

		Student student = new Student();
		student.setData(user);
		student.setYear(5);

		List<Skill> skills = Stream.of(new Skill(UUID.randomUUID().toString(), "Développement web", "Réalisation d'un site en Symfony 4", "Programmation", new ArrayList<>(), List.of("PHP", "Symfony", "MySQL"), 4))
				.collect(Collectors.toList());

		student.setSkills(skills);

		this.studentRepository.save(student);

		User user2 = new User();
		user2.setName("Jane Doe");
		user2.setPassword(passwordDecrypter.crypt("JaneDoe"));
		user2.setEmail("jane.doe@foo.com");
		user2.setRole("Teacher");
		this.userRepository.save(user2);

		Teacher teacher = new Teacher();
		teacher.setData(user2);
		teacher.setSpecialities(List.of("Développement web", "Programmation", "C"));
		this.teacherRepository.save(teacher);
	}

}
