package com.franckbattu.KarutaGraphQL.repositories;

import com.franckbattu.KarutaGraphQL.models.Student;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepository extends MongoRepository<Student, ObjectId> {

    public Student findStudentByData_Name(String name);
    public Student findStudentByData_Email(String email);
    public Student findStudentByData_Id(String id);
}
