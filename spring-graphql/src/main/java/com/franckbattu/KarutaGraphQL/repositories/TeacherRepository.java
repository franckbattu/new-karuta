package com.franckbattu.KarutaGraphQL.repositories;

import com.franckbattu.KarutaGraphQL.models.Teacher;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TeacherRepository extends MongoRepository<Teacher, ObjectId> {

    public Teacher findByData_Email(String email);
}
