package com.franckbattu.KarutaGraphQL.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.franckbattu.KarutaGraphQL.models.Skill;
import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
public class SkillQuery implements GraphQLQueryResolver {

    private StudentRepository studentRepository;

    public SkillQuery(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Skill skillByStudentId(String id, String skillTitle) {
        Student student = this.studentRepository.findById(new ObjectId(id)).orElseThrow();
        return student.getSkills()
                .stream()
                .filter(skill -> skill.getTitle().equals(skillTitle))
                .findFirst()
                .orElseThrow();
    }

}
