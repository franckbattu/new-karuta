package com.franckbattu.KarutaGraphQL.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.franckbattu.KarutaGraphQL.models.Teacher;
import com.franckbattu.KarutaGraphQL.repositories.TeacherRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeacherQuery implements GraphQLQueryResolver {

    private TeacherRepository teacherRepository;

    public TeacherQuery(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public List<Teacher> teachers() {
        return this.teacherRepository.findAll();
    }

    public Teacher teacherByEmail(String email) {
        return this.teacherRepository.findByData_Email(email);
    }
}
