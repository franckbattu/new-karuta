package com.franckbattu.KarutaGraphQL.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.franckbattu.KarutaGraphQL.models.Skill;
import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import com.franckbattu.KarutaGraphQL.security.PasswordDecrypter;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StudentQuery implements GraphQLQueryResolver {

    private StudentRepository studentRepository;

    public StudentQuery(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    /**
     * Returns the list of all students
     *
     * @see Student
     * @return The list of all students
     */
    public List<Student> students() {
        return this.studentRepository.findAll();
    }

    /**
     * Finds a student by its name
     *
     * @param name The name of the student
     * @return The corresponding student
     */
    public Student studentByName(String name) {
        return this.studentRepository.findStudentByData_Name(name);
    }

    /**
     * Finds a student by its email
     *
     * @param email The email of the student
     * @return The corresponding student
     */
    public Student studentByEmail(String email) { return this.studentRepository.findStudentByData_Email(email); }

    /**
     * Finds a student by its id
     *
     * @param id The id of the student
     * @return The corresponding student
     */
    public Student studentById(String id) {
        return this.studentRepository.findStudentByData_Id(id);
    }

}
