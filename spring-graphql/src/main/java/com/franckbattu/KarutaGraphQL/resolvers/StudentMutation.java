package com.franckbattu.KarutaGraphQL.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.franckbattu.KarutaGraphQL.models.Skill;
import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentMutation implements GraphQLMutationResolver {

    private StudentRepository studentRepository;

    public StudentMutation(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

}

