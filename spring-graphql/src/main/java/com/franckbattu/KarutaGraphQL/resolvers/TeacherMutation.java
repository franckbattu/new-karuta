package com.franckbattu.KarutaGraphQL.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.franckbattu.KarutaGraphQL.models.Skill;
import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeacherMutation implements GraphQLMutationResolver {

    private StudentRepository studentRepository;

    public TeacherMutation(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public boolean approveSkill(String idStudent, String titleSkill, String nameTeacher) {
        Student student = this.studentRepository.findById(new ObjectId(idStudent)).orElseThrow();
        for (Skill skill : student.getSkills()) {
            if (skill.getTitle().equals(titleSkill)) {
                skill.getApproved().add(nameTeacher);
                this.studentRepository.save(student);
                return true;
            }
        }
        return false;
    }

    public boolean disapproveSkill(String idStudent, String nameTeacher) {
        Student student = this.studentRepository.findById(new ObjectId(idStudent)).orElseThrow();
        for (Skill skill : student.getSkills()) {
            if (skill.getApproved().contains(nameTeacher)) {
                skill.getApproved().remove(nameTeacher);
                this.studentRepository.save(student);
                return true;
            }
        }
        return false;
    }

}
