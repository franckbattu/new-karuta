package com.franckbattu.KarutaGraphQL.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.franckbattu.KarutaGraphQL.models.User;
import com.franckbattu.KarutaGraphQL.repositories.UserRepository;
import com.franckbattu.KarutaGraphQL.security.PasswordDecrypter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserQuery implements GraphQLQueryResolver {

    private UserRepository userRepository;

    public UserQuery(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> users() {
        return this.userRepository.findAll();
    }

    public User login(String email, String password) {
        User user = this.userRepository.findByEmail(email);
        if (user != null) {
            PasswordDecrypter passwordDecrypter = new PasswordDecrypter();
            if (passwordDecrypter.decrypt(password, user.getPassword())) {
                return user;
            }
        }
        return null;
    }
}
