package com.franckbattu.KarutaGraphQL.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.franckbattu.KarutaGraphQL.models.Skill;
import com.franckbattu.KarutaGraphQL.models.Student;
import com.franckbattu.KarutaGraphQL.repositories.StudentRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Component
public class SkillMutation implements GraphQLMutationResolver {

    private StudentRepository studentRepository;

    public SkillMutation(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Skill addSkill(String id, String title, String description, String category, String technologies, int level) {
        Student student = this.studentRepository.findStudentByData_Id(id);
        Skill skill = new Skill(UUID.randomUUID().toString(), title, description, category, new ArrayList<>(), List.of(technologies.split("[ ,]")), level);
        student.getSkills().add(skill);
        this.studentRepository.save(student);
        return skill;
    }

    public Skill updateSkill(String studentId, String skillId, String title, String description, String category, List<String> technologies, int level) {
        Student student = this.studentRepository.findStudentByData_Id(studentId);
        student.getSkills()
                .stream()
                .filter(s -> s.getId().equals(skillId))
                .findFirst()
                .ifPresent(s -> {
                    s.setTitle(title);
                    s.setDescription(description);
                    s.setCategory(category);
                    s.setTechnologies(technologies);
                    s.setLevel(level);
                });

        this.studentRepository.save(student);

        return student.getSkills().stream().filter(s -> s.getId().equals(skillId)).findFirst().orElseThrow();
    }
}
