package com.franckbattu.KarutaGraphQL.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordDecrypter {

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public PasswordDecrypter() {
        this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }

    public String crypt(String password) {
        return this.bCryptPasswordEncoder.encode(password);
    }

    public boolean decrypt(String password, String userPassword) {
        return this.bCryptPasswordEncoder.matches(password, userPassword);
    }
}
