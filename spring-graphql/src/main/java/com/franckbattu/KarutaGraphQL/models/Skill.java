package com.franckbattu.KarutaGraphQL.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Skill {

    private String id;
    private String title;
    private String description;
    private String category;
    private List<String> approved;
    private List<String> technologies;
    private int level;

}
