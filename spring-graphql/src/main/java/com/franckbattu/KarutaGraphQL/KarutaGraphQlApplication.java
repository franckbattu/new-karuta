package com.franckbattu.KarutaGraphQL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class KarutaGraphQlApplication {

	public static void main(String[] args) {
		SpringApplication.run(KarutaGraphQlApplication.class, args);
	}

}


