import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {StudentGraphqlService} from '../../graphql/student.graphql.service';
import {Student} from '../../../models/student';
import {Teacher} from '../../../models/teacher';
import {TeacherGraphqlService} from '../../graphql/teacher.graphql.service';
import {Skill} from '../../../models/skill';
import {Location} from '@angular/common';

@Component({
  selector: 'app-single-student',
  templateUrl: './single-student.component.html',
  styleUrls: ['./single-student.component.scss']
})
export class SingleStudentComponent implements OnInit {

  student: Student;
  name = '';
  skillsApproved: boolean[] = [];

  constructor(private route: ActivatedRoute, private location: Location, private studentGraphqlService: StudentGraphqlService, private teacherGraphqlService: TeacherGraphqlService) {
  }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.studentGraphqlService.findStudentById(id).subscribe(res => {
      // @ts-ignore
      this.student = res.data.studentById;
      this.name = localStorage.getItem('name');
      this.checkSkills();
    });
  }

  checkSkills() {
    this.student.skills.forEach(skill => {
      this.skillsApproved = [...this.skillsApproved, skill.approved.includes(this.name)];
    });
  }

  approveSkill(skill: Skill) {
    this.teacherGraphqlService.approveSkill(this.student.id, skill.title, this.name).subscribe(res => {
      this.location.back();
    });
  }

  disapproveSkill(skill: Skill) {
    this.teacherGraphqlService.disapproveSkill(this.student.id, this.name).subscribe(res => {
      this.location.back();
    });
  }

}
