import {SingleStudentComponent} from './single-student.component';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TeacherComponent} from '../teacher.component';
import {ApolloTestingModule} from 'apollo-angular/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('SingleStudentComponent', () => {

  let component: SingleStudentComponent;
  let fixture: ComponentFixture<SingleStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SingleStudentComponent],
      imports: [ApolloTestingModule, RouterTestingModule]
    }).compileComponents();
    fixture = TestBed.createComponent(SingleStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should display the button to approve a skill', () => {
    component.skillsApproved = [false];

    component.student = {
      id: '1',
      data: {
        id: '1',
        name: 'John Doe',
        email: 'john.doe@foo.com',
        role: 'Student',
        password: 'JohnDoe',
      },
      year: 5,
      skills: [{
        id: '1',
        title: 'A random skill title',
        description: 'A random skill description',
        approved: [],
        technologies: ['MySQL', 'PHP'],
        level: 3,
        category: 'Web developpement'
      }]
    };

    fixture.detectChanges();

    const btn = fixture.nativeElement.querySelector('.btn');
    expect(btn.textContent).toEqual('Approuver');
  });

  it('should display the button to disapprove a skill', () => {
    component.skillsApproved = [true];

    component.student = {
      id: '1',
      data: {
        id: '1',
        name: 'John Doe',
        email: 'john.doe@foo.com',
        role: 'Student',
        password: 'JohnDoe',
      },
      year: 5,
      skills: [{
        id: '1',
        title: 'A random skill title',
        description: 'A random skill description',
        approved: [],
        technologies: ['MySQL', 'PHP'],
        level: 3,
        category: 'Web developpement'
      }]
    };

    fixture.detectChanges();

    const btn = fixture.nativeElement.querySelector('.btn');
    expect(btn.textContent).toEqual('Désapprouver');
  });

});
