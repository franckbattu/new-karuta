import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ApolloTestingModule} from 'apollo-angular/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TeacherComponent} from './teacher.component';

describe('TeacherComponent', () => {

  let component: TeacherComponent;
  let fixture: ComponentFixture<TeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeacherComponent],
      imports: [ApolloTestingModule, RouterTestingModule]
    }).compileComponents();
    fixture = TestBed.createComponent(TeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display teacher data', () => {
    component.teacher = {
      id: '1',
      data: {
        id: '1',
        name: 'Jane Doe',
        email: 'jane.doe@foo.com',
        role: 'Teacher',
        password: 'JaneDoe',
      },
      specialities: ['Développement web', 'BigData', 'Software']
    };

    fixture.detectChanges();
    const h1 = fixture.nativeElement.querySelector('h1');
    const h6List = fixture.nativeElement.querySelectorAll('h6');
    expect(h1.textContent).toEqual('Bienvenue Jane Doe');
    expect(h6List[0].textContent).toEqual('Nom et prénom : Jane Doe');
    expect(h6List[1].textContent).toEqual('Email : jane.doe@foo.com');
  });
});
