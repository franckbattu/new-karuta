import { Component, OnInit } from '@angular/core';
import {Teacher} from '../../models/teacher';
import {TeacherGraphqlService} from '../graphql/teacher.graphql.service';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss']
})
export class TeacherComponent implements OnInit {

  teacher: Teacher;

  constructor(private teacherGraphqlService: TeacherGraphqlService) { }

  ngOnInit() {
    const email = localStorage.getItem('email');
    this.teacherGraphqlService.findTeacherByEmail(email).subscribe(res => {
      // @ts-ignore
      this.teacher = res.data.teacherByEmail;
    });
  }

}
