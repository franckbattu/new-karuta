import { Component, OnInit } from '@angular/core';
import {StudentGraphqlService} from '../../graphql/student.graphql.service';
import {Student} from '../../../models/student';
import {Router} from '@angular/router';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  students: Student[] = [];

  constructor(private studentGraphqlService: StudentGraphqlService, private router: Router) { }

  ngOnInit() {
    this.studentGraphqlService.findAll().subscribe(res => {
      // @ts-ignore
      this.students = res.data.students;
    });
  }

  onClickCheckSkills(student: Student) {
    this.router.navigate([`/students/${student.data.id}`]);
  }

}
