import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {ApolloTestingModule} from 'apollo-angular/testing';
import {Student} from '../../models/student';
import {RouterTestingModule} from '@angular/router/testing';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent],
      imports: [ApolloTestingModule, RouterTestingModule]
    }).compileComponents();
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display title with student name', () => {
    component.student = {
      id: '1',
      data: {
        id: '1',
        name: 'John Doe',
        email: 'john.doe@foo.com',
        role: 'Student',
        password: 'JohnDoe',
      },
      year: 5,
      skills: []
    };
    fixture.detectChanges();
    const h1 = fixture.nativeElement.querySelector('h1');
    expect(h1.textContent).toEqual('Bienvenue John Doe');
  });
});
