import {Component, OnInit} from '@angular/core';
import {Student} from '../../models/student';
import {StudentGraphqlService} from '../graphql/student.graphql.service';
import {Router} from '@angular/router';
import {Skill} from '../../models/skill';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  student: Student;

  constructor(private studentGraphqlService: StudentGraphqlService, private router: Router) {
  }

  ngOnInit() {
    const id = localStorage.getItem('id');
    this.studentGraphqlService.findStudentById(id).subscribe(res => {
      // @ts-ignore
      this.student = res.data.studentById;
    });
  }

  updateSkill(skill: Skill) {
    this.router.navigate([`/update`], {
      state: {
        data: {
          skill
        }
      }
    });
  }
}
