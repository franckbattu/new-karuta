import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StudentGraphqlService} from '../graphql/student.graphql.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private studentGraphqlService: StudentGraphqlService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      category: ['', Validators.required],
      technologies: ['', Validators.required],
      level: [1, Validators.required]
    });
  }

  onSubmitForm() {
    const {title, description, category, technologies, level } = this.form.value;
    this.studentGraphqlService.addSkill(title, description, category, technologies, +level).subscribe(res => {
      this.form.reset();
      this.router.navigate(['/home']);
    });
  }
}
