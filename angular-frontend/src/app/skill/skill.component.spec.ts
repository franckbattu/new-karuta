import {SkillComponent} from './skill.component';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LoginComponent} from '../login/login.component';
import {ApolloTestingModule} from 'apollo-angular/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

describe('SkillComponent', () => {

  let component: SkillComponent;
  let fixture: ComponentFixture<SkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillComponent ],
      imports: [ApolloTestingModule, ReactiveFormsModule, RouterTestingModule]
    }).compileComponents();
    fixture = TestBed.createComponent(SkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('the form button should be disabled', () => {
    const submitEL = fixture.debugElement.nativeElement.querySelector('button[type=submit]');
    expect(submitEL.disabled).toEqual(true);
  });

  it('the form is not valid when empty', () => {
    expect(component.form.valid).not.toEqual(true);
  });

  it('the form button should be enabled when form correct', () => {

    component.form.controls.title.setValue('A random skill title');
    component.form.controls.description.setValue('A random skill description');
    component.form.controls.category.setValue('A random skill category');
    component.form.controls.technologies.setValue('RandomTechnology1 RandomTechnology2');
    component.form.controls.level.setValue(1);

    fixture.detectChanges();
    const submitEL = fixture.debugElement.nativeElement.querySelector('button[type=submit]');
    expect(submitEL.disabled).not.toEqual(true);
  });

});
