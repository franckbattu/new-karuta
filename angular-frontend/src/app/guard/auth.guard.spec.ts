import { TestBed, async, inject } from '@angular/core/testing';

import { StudentGuard } from './student.guard';
import {
  ApolloTestingModule,
  ApolloTestingController,
} from 'apollo-angular/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentGuard],
      imports: [ApolloTestingModule, RouterTestingModule]
    });
  });

  it('should ...', inject([StudentGuard], (guard: StudentGuard) => {
    expect(guard).toBeTruthy();
  }));
});
