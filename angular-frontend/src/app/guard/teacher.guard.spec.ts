import { TestBed, async, inject } from '@angular/core/testing';

import { TeacherGuard } from './teacher.guard';
import {
  ApolloTestingModule,
} from 'apollo-angular/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('TeacherGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherGuard],
      imports: [ApolloTestingModule, RouterTestingModule]
    }).compileComponents();
  });

  it('should ...', inject([TeacherGuard], (guard: TeacherGuard) => {
    expect(guard).toBeTruthy();
  }));
});
