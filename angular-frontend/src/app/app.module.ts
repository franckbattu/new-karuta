import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {NavbarComponent} from './navbar/navbar.component';
import {APOLLO_OPTIONS, ApolloModule} from 'apollo-angular';
import {HttpLink, HttpLinkModule} from 'apollo-angular-link-http';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {SkillComponent} from './skill/skill.component';
import {TeacherComponent} from './teacher/teacher.component';
import {StudentsComponent} from './teacher/students/students.component';
import {SingleStudentComponent} from './teacher/single-student/single-student.component';
import { UpdateSkillComponent } from './update-skill/update-skill.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    SkillComponent,
    TeacherComponent,
    StudentsComponent,
    SingleStudentComponent,
    UpdateSkillComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApolloModule,
    HttpLinkModule,
    HttpClientModule,
    ReactiveFormsModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: (httpLink: HttpLink) => {
        return {
          cache: new InMemoryCache(),
          link: httpLink.create({
            uri: 'http://localhost:8080/graphql'
          }),
          defaultOptions: {
            query: {
              fetchPolicy: 'no-cache'
            }
          }
        };
      },
      deps: [HttpLink]
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
