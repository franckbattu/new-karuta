import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Skill} from '../../models/skill';
import {StudentGraphqlService} from '../graphql/student.graphql.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-update-skill',
  templateUrl: './update-skill.component.html',
  styleUrls: ['./update-skill.component.scss']
})
export class UpdateSkillComponent implements OnInit {

  form: FormGroup;
  skill: Skill;
  studentId: number;

  constructor(private formBuilder: FormBuilder, private router: Router, private studentGraphqlService: StudentGraphqlService) { }

  ngOnInit() {
    this.skill = history.state.data.skill;
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      title: [this.skill.title, Validators.required],
      description: [this.skill.description, Validators.required],
      category: [this.skill.category, Validators.required],
      technologies: [this.skill.technologies, Validators.required],
      level: [this.skill.level, Validators.required]
    });
  }

  onSubmitForm() {
    const { title, description, category, technologies, level } = this.form.value;
    this.studentGraphqlService.updateSkill(this.skill.id, title, description, category, technologies, level).subscribe(res => {
      this.router.navigate(['/home']);
    });
  }

}
