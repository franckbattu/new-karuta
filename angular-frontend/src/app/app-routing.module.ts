import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {StudentGuard} from './guard/student.guard';
import {SkillComponent} from './skill/skill.component';
import {TeacherComponent} from './teacher/teacher.component';
import {TeacherGuard} from './guard/teacher.guard';
import {StudentsComponent} from './teacher/students/students.component';
import {SingleStudentComponent} from './teacher/single-student/single-student.component';
import {UpdateSkillComponent} from './update-skill/update-skill.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [StudentGuard] },
  { path: 'skill', component: SkillComponent, canActivate: [StudentGuard] },
  { path: 'update', component: UpdateSkillComponent, canActivate: [StudentGuard] },
  { path: 'teacher', component: TeacherComponent, canActivate: [TeacherGuard] },
  { path: 'students', component: StudentsComponent, canActivate: [TeacherGuard] },
  { path: 'students/:id', component: SingleStudentComponent, canActivate: [TeacherGuard] },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
