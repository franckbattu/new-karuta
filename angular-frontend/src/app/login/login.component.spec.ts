import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {ApolloTestingModule} from 'apollo-angular/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ApolloTestingModule, ReactiveFormsModule, RouterTestingModule]
    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('the form button should be disabled', () => {
    const submitEL = fixture.debugElement.nativeElement.querySelector('button[type=submit]');
    expect(submitEL.disabled).toEqual(true);
  });

  it('the form is not valid when empty', () => {
    expect(component.form.valid).not.toEqual(true);
  });

  it('the form button should be activated when form correct', () => {
    component.form.controls.email.setValue('john.doe@foo.com');
    component.form.controls.password.setValue('JohnDoe');
    fixture.detectChanges();
    const submitEL = fixture.debugElement.nativeElement.querySelector('button[type=submit]');
    expect(submitEL.disabled).not.toEqual(true);
  });
});
