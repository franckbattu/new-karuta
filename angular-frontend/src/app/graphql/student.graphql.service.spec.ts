import {TestBed} from '@angular/core/testing';

import {
  FIND_ALL_STUDENT_QUERY,
  FIND_ONE_STUDENT_BY_EMAIL_QUERY,
  FIND_ONE_STUDENT_BY_ID_QUERY,
  StudentGraphqlService
} from './student.graphql.service';
import {APOLLO_TESTING_CACHE, ApolloTestingController, ApolloTestingModule} from 'apollo-angular/testing';
import {addTypenameToDocument} from 'apollo-utilities';
import {InMemoryCache} from 'apollo-cache-inmemory';

describe('Student.GraphqlService', () => {

  let controller: ApolloTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [ApolloTestingModule],
    providers: [
      {
        provide: APOLLO_TESTING_CACHE,
        useValue: new InMemoryCache({addTypename: true}),
      },
    ],
  }).compileComponents());

  beforeEach(() => {
    controller = TestBed.get(ApolloTestingController);
  });

  it('should be created', () => {
    const service = TestBed.get(StudentGraphqlService);
    expect(service).toBeTruthy();
  });

  it('should find all students', (done) => {
    const service = TestBed.get(StudentGraphqlService);
    service.findAll().subscribe((res) => {
      // @ts-ignore
      const student1 = res.data.students[0];
      // @ts-ignore
      const student2 = res.data.students[1];
      expect(student1.data.name).toEqual('John Doe');
      expect(student1.year).toEqual(5);
      expect(student2.data.name).toEqual('Jane Doe');
      expect(student2.year).toEqual(4);
      done();
    });
    const op = controller.expectOne(addTypenameToDocument(FIND_ALL_STUDENT_QUERY));
    op.flush({
      data: {
        students: [
          {
            id: 1,
            data: {
              id: '1',
              name: 'John Doe',
              email: 'john.doe@foo.com',
              __typename: 'Student'
            },
            year: 5,
            __typename: 'Student'
          }, {
            id: 2,
            data: {
              id: '2',
              name: 'Jane Doe',
              email: 'jane.doe@foo.com',
              __typename: 'Student'
            },
            year: 4,
            __typename: 'Student'
          }
        ]
      }
    });
  });

  it('should find one student by id', (done) => {
    const service = TestBed.get(StudentGraphqlService);
    service.findStudentById('1').subscribe(res => {
      const student = res.data.studentById;
      expect(student.data.name).toEqual('John Doe');
      expect(student.skills[0].level).toEqual(2);
      done();
    });

    const op = controller.expectOne(addTypenameToDocument(FIND_ONE_STUDENT_BY_ID_QUERY));
    op.flush({
      data: {
        studentById: {
          id: 1,
          data: {
            id: '1',
            name: 'John Doe',
            email: 'john.doe@foo.com',
            __typename: 'Data'
          },
          year: 5,
          skills: [
            {
              id: '1',
              title: 'Web developpement',
              description: 'Good knowledges in web developpement',
              category: 'Coding',
              technologies: ['PHP', 'MySQL'],
              level: 2,
              approved: [],
              __typename: 'Skill'
            }],
          __typename: 'Student'
        }
      }
    });
  });

  it('should find one student by email', (done) => {
    const service = TestBed.get(StudentGraphqlService);
    service.findStudentByEmail('john.doe@foo.com').subscribe(res => {
      const student = res.data.studentByEmail;
      expect(student.data.name).toEqual('John Doe');
      expect(student.skills[0].level).toEqual(2);
      done();
    });

    const op = controller.expectOne(addTypenameToDocument(FIND_ONE_STUDENT_BY_EMAIL_QUERY));
    op.flush({
      data: {
        studentByEmail: {
          id: 1,
          data: {
            id: '1',
            name: 'John Doe',
            email: 'john.doe@foo.com',
            __typename: 'Data'
          },
          year: 5,
          skills: [
            {
              id: '1',
              title: 'Web developpement',
              description: 'Good knowledges in web developpement',
              category: 'Coding',
              technologies: ['PHP', 'MySQL'],
              level: 2,
              approved: [],
              __typename: 'Skill'
            }],
          __typename: 'Student'
        }
      }
    });
  });

  afterEach(() => {
    controller.verify();
  });
});
