import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {Skill} from '../../models/skill';


export const FIND_ALL_STUDENT_QUERY = gql`query { students { id data { id name email } year } }`;
// tslint:disable-next-line:max-line-length
export const FIND_ONE_STUDENT_BY_ID_QUERY = gql`query getStudentById($id: String) { studentById(id: $id) { id data { id name email } year skills { id title description category technologies level approved } }}`;
// tslint:disable-next-line:max-line-length
export const FIND_ONE_STUDENT_BY_EMAIL_QUERY = gql`query getStudentByEmail($email: String) { studentByEmail(email: $email) { id data { name email } year skills { id title description category approved technologies level }  }}`;
// tslint:disable-next-line:max-line-length
export const ADD_ONE_SKILL = gql`mutation addSkill($id: String, $title: String, $description: String, $category: String, $technologies: String, $level: Int) { addSkill(id: $id, title: $title, description: $description, category: $category, technologies: $technologies, level: $level) { title }}`;
// tslint:disable-next-line:max-line-length
export const UPDATE_ONE_SKILL = gql`mutation updateSkill($studentId: String, $skillId: String, $title: String, $description: String, $category: String, $technologies: [String], $level: Int) { updateSkill(studentId: $studentId, skillId: $skillId, title: $title, description: $description, category: $category, technologies: $technologies, level: $level) { title }}`;

@Injectable({
  providedIn: 'root'
})
export class StudentGraphqlService {

  constructor(private apollo: Apollo) {
  }

  findAll() {
    return this.apollo.query({
      query: FIND_ALL_STUDENT_QUERY
    });
  }

  findStudentById(id: string) {
    return this.apollo.query({
      query: FIND_ONE_STUDENT_BY_ID_QUERY,
      variables: {id}
    });
  }

  findStudentByEmail(email: string) {
    return this.apollo.query({
      query: FIND_ONE_STUDENT_BY_EMAIL_QUERY,
      variables: {email}
    });
  }

  addSkill(title: string, description: string, category: string, technologies: string, level: number) {
    return this.apollo.mutate({
      // tslint:disable-next-line:max-line-length
      mutation: ADD_ONE_SKILL,
      variables: {
        id: localStorage.getItem('id'),
        title,
        description,
        category,
        technologies,
        level
      }
    });
  }

  updateSkill(skillId: string, title: string, description: string, category: string, technologies: string[], level: number) {
    return this.apollo.mutate({
      mutation: UPDATE_ONE_SKILL,
      variables: {
        studentId: localStorage.getItem('id'),
        skillId,
        title,
        description,
        category,
        technologies,
        level
      }
    });
  }
}
