import { Injectable } from '@angular/core';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';

// tslint:disable-next-line:max-line-length
export const FIND_ONE_TEACHER_BY_EMAIL = gql`query getTeacherByEmail($email: String) { teacherByEmail(email: $email) { data { name email } specialities }}`;

@Injectable({
  providedIn: 'root'
})
export class TeacherGraphqlService {

  constructor(private apollo: Apollo) { }

  findTeacherByEmail(email: string) {
    return this.apollo.query({
      query: FIND_ONE_TEACHER_BY_EMAIL,
      variables: {email}
    });
  }


  approveSkill(id: string, title: string, name: string) {
    return this.apollo.mutate({
      mutation: gql`mutation { approveSkill(id: "${id}", title: "${title}", name: "${name}") }`
    });
  }

  disapproveSkill(idStudent: string, nameTeacher: string) {
    return this.apollo.mutate({
      mutation: gql`mutation { disapproveSkill(id: "${idStudent}", name: "${nameTeacher}") }`
    });
  }
}
