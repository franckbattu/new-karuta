import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

// tslint:disable-next-line:max-line-length
export const LOGIN_QUERY = gql`query login($email: String, $password: String) { login(email: $email, password: $password) { id name email role }}`;

@Injectable({
  providedIn: 'root'
})
export class AuthGraphqlService {

  constructor(private apollo: Apollo) {
  }

  login(email: string, password: string) {
    return this.apollo.query({
      query: LOGIN_QUERY,
      variables: {email, password}
    });
  }
}
