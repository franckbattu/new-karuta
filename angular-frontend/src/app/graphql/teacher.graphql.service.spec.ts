import { TestBed } from '@angular/core/testing';
import {addTypenameToDocument} from 'apollo-utilities';
import {FIND_ONE_TEACHER_BY_EMAIL, TeacherGraphqlService} from './teacher.graphql.service';
import {
  ApolloTestingModule,
  ApolloTestingController, APOLLO_TESTING_CACHE,
} from 'apollo-angular/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {InMemoryCache} from 'apollo-cache-inmemory';

describe('TeacherGraphqlService', () => {

  let controller: ApolloTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [ApolloTestingModule],
    providers: [
      {
        provide: APOLLO_TESTING_CACHE,
        useValue: new InMemoryCache({addTypename: true}),
      },
    ],
  }).compileComponents());

  beforeEach(() => {
    controller = TestBed.get(ApolloTestingController);
  })

  it('should be created', () => {
    const service: TeacherGraphqlService = TestBed.get(TeacherGraphqlService);
    expect(service).toBeTruthy();
  });

  it('should find one teacher by email', done => {
    const service = TestBed.get(TeacherGraphqlService);
    service.findTeacherByEmail('jane.doe@foo.com').subscribe(res => {
      const teacher = res.data.teacherByEmail;
      expect(teacher.data.name).toEqual('Jane Doe');
      expect(teacher.data.email).toEqual('jane.doe@foo.com');
      expect(teacher.specialities.length).toEqual(0);
      done();
    });

    const op = controller.expectOne(addTypenameToDocument(FIND_ONE_TEACHER_BY_EMAIL));
    op.flush({
      data: {
        teacherByEmail: {
          id: 1,
          data: {
            id: '1',
            name: 'Jane Doe',
            email: 'jane.doe@foo.com',
            __typename: 'TeacherData'
          },
          specialities: [],
          __typename: 'Teacher'
        }
      }
    });
  });

  afterEach(() => {
    controller.verify();
  });
});
