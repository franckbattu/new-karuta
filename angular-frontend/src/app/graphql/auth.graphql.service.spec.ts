import { TestBed } from '@angular/core/testing';
import {APOLLO_TESTING_CACHE, ApolloTestingController, ApolloTestingModule} from 'apollo-angular/testing';

import {AuthGraphqlService, LOGIN_QUERY} from './auth.graphql.service';
import {addTypenameToDocument} from 'apollo-utilities';
import {InMemoryCache} from 'apollo-cache-inmemory';

describe('AuthGraphqlService', () => {

  let controller: ApolloTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [ApolloTestingModule],
    providers: [
      {
        provide: APOLLO_TESTING_CACHE,
        useValue: new InMemoryCache({addTypename: true}),
      },
    ],
  }).compileComponents());

  beforeEach(() => {
    controller = TestBed.get(ApolloTestingController);
  });

  it('should be created', () => {
    const service: AuthGraphqlService = TestBed.get(AuthGraphqlService);
    expect(service).toBeTruthy();
  });

  it('should login', (done) => {
    const service: AuthGraphqlService = TestBed.get(AuthGraphqlService);
    service.login('john.doe@foo.com', 'JohnDoe').subscribe(res => {
      // @ts-ignore
      const student = res.data.login;
      expect(student.name).toEqual('John Doe');
      expect(student.role).toEqual('Student');
    });
    done();

    const op = controller.expectOne(addTypenameToDocument(LOGIN_QUERY));
    op.flush({
      data: {
        login: {
          id: '1',
          name: 'John Doe',
          email: 'john.doe@foo.com',
          role: 'Student',
          __typename: 'Login'
        }
      }
    });
  });
});
