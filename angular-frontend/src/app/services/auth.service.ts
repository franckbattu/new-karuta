import {Injectable} from '@angular/core';
import {AuthGraphqlService} from '../graphql/auth.graphql.service';
import {Router} from '@angular/router';
import {StudentGraphqlService} from '../graphql/student.graphql.service';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authenticated: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private authGraphqlService: AuthGraphqlService, private studentGraphqlService: StudentGraphqlService, private router: Router) {
  }

  login(email: string, password: string) {
    this.authGraphqlService.login(email, password).subscribe(res => {
      // @ts-ignore
      if (res.data.login) {
        // @ts-ignore

        // @ts-ignore
        localStorage.setItem('role', res.data.login.role);
        // @ts-ignore
        localStorage.setItem('email', res.data.login.email);
        // @ts-ignore
        localStorage.setItem('name', res.data.login.name);
        // @ts-ignore
        localStorage.setItem('id', res.data.login.id);
        // @ts-ignore
        switch (res.data.login.role) {
          case 'Student':
            this.router.navigate(['/home']);
            break;
          case 'Teacher':
            this.router.navigate(['/teacher']);
            break;
        }
        this.sucessLogin();
      }
    });
  }

  sucessLogin() {
    this.authenticated.next(true);
  }

  logout() {
    localStorage.clear();
    this.authenticated.next(false);
  }

  isLogged() {
    return this.authenticated;
  }

  getRole() {
    return localStorage.getItem('role');
  }


}
