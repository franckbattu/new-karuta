import {Skill} from './skill';
import {User} from './user';

export interface Student {

  id: string;
  data: User;
  year: number;
  skills: Skill[];

}
