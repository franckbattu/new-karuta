export interface Skill {
  id: string;
  title: string;
  description: string;
  category: string;
  approved: string[];
  technologies: string[];
  level: number;
}

