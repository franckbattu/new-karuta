import {User} from './user';

export interface Teacher {
  id: string;
  data: User;
  specialities: string[];
}
