import {by, element} from 'protractor';

export class SingleStudent {

  getButtons() {
    return element.all(by.css('.btn'));
  }

}
