import {browser, by, element} from 'protractor';

export class Login {

  navigateTo() {
    return browser.get('/');
  }

  getEmailBox() {
    return element(by.id('email'));
  }

  getPasswordBox() {
    return element(by.id('password'));
  }

  getButtonSubmit() {
    return element(by.id('submitButton'));
  }

}
