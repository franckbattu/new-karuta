import {by, element} from 'protractor';

export class Students {

  getCells() {
    const table = element.all(by.tagName('table'));
    const rows = table.all(by.tagName('tr'));
    return rows.all(by.tagName('td'));
  }

}
