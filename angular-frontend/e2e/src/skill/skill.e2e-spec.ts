import {Skill} from './skill.po';
import {Login} from '../login/login.po';
import {Home} from '../home/home.po';

describe('Skill page', () => {

  let skill: Skill;
  let login: Login;
  let home: Home;

  beforeEach(() => {
    home = new Home();
    skill = new Skill();

    login = new Login();
    login.getEmailBox().sendKeys('john.doe@foo.com');
    login.getPasswordBox().sendKeys('JohnDoe');
    login.getButtonSubmit().click();

    home.getSkillsButton().click();
  });

  it('should add a new skill', () => {
    skill.getTitleInput().sendKeys('A random title');
    skill.getDescriptionInput().sendKeys('A random description to test');
    skill.getCategoryInput().sendKeys('Test');
    skill.getTechnologiesInput().sendKeys('Protractor');
    skill.getButtonSubmit().click();

    const cells = home.getCells();
    expect(cells.get(8).getText()).toEqual('A random title');
    expect(cells.get(9).getText()).toEqual('A random description to test');
    expect(cells.get(10).getText()).toEqual('Test');
    expect(cells.get(11).getText()).toEqual('Protractor');
    expect(cells.get(12).getText()).toEqual('1');
    expect(cells.get(13).getText()).toEqual('');
  });


  afterEach(() => {
    home.getLogoutButton().click();
  });
});
