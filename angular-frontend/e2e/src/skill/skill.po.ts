import {browser, by, element} from 'protractor';

export class Skill {

  navigateTo() {
    return browser.get('/skill');
  }

  getTitleInput() {
    return element(by.id('title'));
  }

  getDescriptionInput() {
    return element(by.id('description'));
  }

  getCategoryInput() {
    return element(by.id('category'));
  }

  getTechnologiesInput() {
    return element(by.id('technologies'));
  }

  getButtonSubmit() {
    return element(by.id('buttonSubmit'));
  }


}
