import {Home} from '../home/home.po';
import {Login} from '../login/login.po';
import {UpdateSkill} from './update-skill.po';

describe('Update skill page', () => {

  let home: Home;
  let login: Login;
  let updateSkill: UpdateSkill;

  beforeEach(() => {
    home = new Home();
    updateSkill = new UpdateSkill();

    login = new Login();
    login.getEmailBox().sendKeys('john.doe@foo.com');
    login.getPasswordBox().sendKeys('JohnDoe');
    login.getButtonSubmit().click();
  });

  it('should update the title of a skill', () => {
    home.getFirstIcon().click();
    updateSkill.getTitleInput().clear().then(() => {
      updateSkill.getTitleInput().sendKeys('A new random title');
      updateSkill.getButtonSubmit().click();
      expect(home.getCells().get(1).getText()).toEqual('A new random title');
    });
  });
});
