import {by, element} from 'protractor';

export class UpdateSkill {

  getTitleInput() {
    return element(by.id('title'));
  }

  getDescriptionInput() {
    return element(by.id('description'));
  }

  getCategoryInput() {
    return element(by.id('category'));
  }

  getTechnologiesInput() {
    return element(by.id('technologies'));
  }

  getButtonSubmit() {
    return element(by.id('buttonSubmit'));
  }

}
