import {browser, by, element} from 'protractor';

export class Home {

  navigateToHome() {
    return browser.get('/home');
  }

  getNavbar() {
    return element(by.tagName('nav'));
  }

  getProfileButton() {
    return this.getNavbar().all(by.css('a')).get(1);
  }

  getSkillsButton() {
    return this.getNavbar().all(by.css('a')).get(2);
  }

  getLogoutButton() {
    return this.getNavbar().all(by.css('a#logoutStudent'));
  }

  getCells() {
    const table = element.all(by.tagName('table'));
    const rows = table.all(by.tagName('tr'));
    return rows.all(by.tagName('td'));
  }

  getFirstIcon() {
    return this.getCells().get(0).$('i');
  }

}
