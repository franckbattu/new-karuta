import {Home} from './home.po';
import {Login} from '../login/login.po';

describe('Home page', () => {

  let home: Home;
  let login: Login;

  beforeEach(() => {
    home = new Home();
    home.navigateToHome();

    login = new Login();
    login.getEmailBox().sendKeys('john.doe@foo.com');
    login.getPasswordBox().sendKeys('JohnDoe');
    login.getButtonSubmit().click();
  });

  it('should have a profile button in navbar', () => {
    expect(home.getProfileButton().getText()).toEqual('Profil');
  });

  it('should have a skill button in navbar', () => {
    expect(home.getSkillsButton().getText()).toEqual('Ajouter une compétence');
  });

  afterEach(() => {
    home.getLogoutButton().click();
  });

});
