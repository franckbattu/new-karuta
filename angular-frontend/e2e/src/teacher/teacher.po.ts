import {browser, by, element} from 'protractor';

export class Teacher {

  navigateToHome() {
    return browser.get('/teacher');
  }

  getNavbar() {
    return element(by.tagName('nav'));
  }

  getListOfStudents() {
    return this.getNavbar().all(by.css('a')).get(1);
  }

  getLogoutButton() {
    return this.getNavbar().all(by.css('a#logoutTeacher'));
  }

}
