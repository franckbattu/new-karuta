import {Teacher} from './teacher.po';
import {Login} from '../login/login.po';
import {Students} from '../students/students.po';
import {SingleStudent} from '../single-student/single-student.po';

describe('Teacher page', () => {

  let teacher: Teacher;
  let students: Students;

  beforeEach(() => {
    teacher = new Teacher();
    const login = new Login();
    login.getEmailBox().sendKeys('jane.doe@foo.com');
    login.getPasswordBox().sendKeys('JaneDoe');
    login.getButtonSubmit().click();
  });

  it('should display the list of students button', () => {
    expect(teacher.getListOfStudents().getText()).toEqual('Liste des étudiants');
  });

  it('should approve a student skill', () => {
      teacher.getListOfStudents().click();
      students = new Students();
      students.getCells().get(3).click();
      const singleStudent = new SingleStudent();

      expect(singleStudent.getButtons().get(0).getText()).toEqual('Approuver');
      singleStudent.getButtons().get(0).click();

      students.getCells().get(3).click();
      expect(singleStudent.getButtons().get(0).getText()).toEqual('Désapprouver');
  });

  afterEach(() => {
    teacher.getLogoutButton().click();
  });

});
